package com.raulguidelines.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

/**
 * Created by raul.metsva on 31.03.2017.
 */
@Entity
@Table(name = "grouping")
public class Grouping {


    private Integer groupingId;

    @NotNull
    private String groupingName;

    @JsonFormat(pattern="dd-MM-yyyy")
    @NotNull
    private Date groupingCreate;

    @JsonFormat(pattern="dd-MM-yyyy")
    @NotNull
    private Date groupingModify;

    private Set<User> users;

    public Grouping() {
    }

    public Grouping(String groupingName) {
        this.groupingName = groupingName;
        this.groupingCreate = new Date();
        this.groupingModify = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getGroupingId() {
        return groupingId;
    }

    public void setGroupingId(Integer groupingId) {
        this.groupingId = groupingId;
    }

    public String getGroupingName() {
        return groupingName;
    }

    public void setGroupingName(String groupingName) {
        this.groupingName = groupingName;
    }

    public Date getGroupingCreate() {
        return groupingCreate;
    }

    public void setGroupingCreate(Date groupingCreate) {
        this.groupingCreate = groupingCreate;
    }

    public Date getGroupingModify() {
        return groupingModify;
    }

    public void setGroupingModify(Date groupingModify) {
        this.groupingModify = groupingModify;
    }


    @ManyToMany(mappedBy = "groupings", cascade = CascadeType.ALL)
    @JsonManagedReference
    public Set<User> getUsers() {
        return users;
    }
    public void setUsers(Set<User> users) {
        this.users = users;
    }
}

