package com.raulguidelines.models;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by raul.metsva on 20.04.2017.
 */
public interface MyFileDao extends CrudRepository<MyFile, Integer> {
    public MyFile findByPath (String path);
}
