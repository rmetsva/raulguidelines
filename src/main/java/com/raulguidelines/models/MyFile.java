package com.raulguidelines.models;

import com.raulguidelines.controllers.FileUploadController;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by raul.metsva on 17.04.2017.
 */

@Entity
@Table(name = "myFile")
public class MyFile {

    private Integer myFileId;

    private Double fileSize;

    @NotNull
    private String path;

    @NotNull
    @Size(min=5, max=50)
    private String description;

    private String sizeAsString;

    public MyFile() {
    }

    public MyFile(File file, Path rootLocation, String description) throws IOException {
        this.fileSize = (double) file.length();
        this.description = description;

        if(!(Paths.get(file.getAbsolutePath()).equals(rootLocation))) {
            this.path = MvcUriComponentsBuilder
                    .fromMethodName(FileUploadController.class, "serveFile", Paths.get(file.getAbsolutePath()).getFileName().toString())
                    .build().toString();
        } else {
            this.path = "null";
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getMyFileId() {
        return myFileId;
    }

    public void setMyFileId(Integer myFileId) {
        this.myFileId = myFileId;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(Double fileSize) {
        this.fileSize = fileSize;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSizeAsString() {
        String a = "";
        if(this.fileSize < 1024) {
            a = Math.round(this.fileSize) + " bytes";
        } else if (this.fileSize < 1024*1024) {
            a = Math.round(this.fileSize / 1024) + " KB";
        }
        return a;
    }

    public void setSizeAsString(String sizeAsString) {
        this.sizeAsString = sizeAsString;
    }

}
