package com.raulguidelines.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

/**
 * Created by raul.metsva on 31.03.2017.
 */
@Entity
@Table(name = "user")
public class User {


    private Integer userId;

    @NotNull
    @Size(min=5, max=30)
    private String userName;

    @NotNull
    @Email
    private String userEmail;

    @JsonFormat(pattern="dd-MM-yyyy")
    @NotNull
    private Date userBirthday;

    @JsonFormat(pattern="dd-MM-yyyy")
    @NotNull
    private Date userCreate;

    @JsonFormat(pattern="dd-MM-yyyy")
    @NotNull
    private Date userModify;

    private Set<Grouping> groupings;

    public User() {
    }

    public User(String userName, String userEmail, Date userBirthday) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userBirthday = userBirthday;
        this.userCreate = new Date();
        this.userModify = new Date();
    }

    public User(Integer userId, String userName, String userEmail, Date userBirthday, Date userCreate, Date userModify) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userBirthday = userBirthday;
        this.userCreate = userCreate;
        this.userModify = userModify;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Date getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(Date userCreate) {
        this.userCreate = userCreate;
    }

    public Date getUserModify() {
        return userModify;
    }

    public void setUserModify(Date userModify) {
        this.userModify = userModify;
    }


    @ManyToMany (cascade =
            {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.REFRESH,
                    CascadeType.PERSIST
            })
    @JoinTable (name="users_groupings", joinColumns = @JoinColumn(name="user_id"), inverseJoinColumns = @JoinColumn(name="grouping_id"))
    @JsonBackReference
    public Set<Grouping> getGroupings() {
        return groupings;
    }
    public void setGroupings(Set<Grouping> groupings) {
        this.groupings = groupings;
    }
}
