package com.raulguidelines.models;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by raul.metsva on 31.03.2017.
 */
public interface GroupingDao extends CrudRepository<Grouping, Integer> {
    public Grouping findByGroupingName (String groupingName);

}
