package com.raulguidelines.controllers;

import com.raulguidelines.models.Grouping;
import com.raulguidelines.models.GroupingDao;
import com.raulguidelines.models.User;
import com.raulguidelines.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by raul.metsva on 31.03.2017.
 */
@Controller
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private GroupingDao groupingDao;

    @CrossOrigin(origins = "*")
    @RequestMapping("/userlist")
    @ResponseBody
    public Iterable<User> getAllUsers() {
        return userDao.findAll();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/create-user")
    @ResponseBody
    public void createUser(String userName, String userEmail, String userBirthday) throws Exception {
        Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(userBirthday);
        User user = new User (userName, userEmail, date1);
        userDao.save(user);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/update-user")
    @ResponseBody
    public void updateUser (String userName, String newUserName, String userEmail) {
        //Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(userBirthday);
        User user = userDao.findByUserName(userName);
        user.setUserName(newUserName);
        user.setUserEmail(userEmail);
        //user.setUserBirthday(date1);
        //user.setUserModify(userModify);
        userDao.save(user);
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete-user")
    @ResponseBody
    public void deleteUser (String userName) {
        User user = userDao.findByUserName(userName);
        userDao.delete(user);
    }

    @CrossOrigin(origins = "*")
    @PostMapping ("/add-grouping-to-user")
    @ResponseBody
    public void addGroupingToUser (String userName, String groupingName){
        User user = userDao.findByUserName(userName);
        Grouping grouping = groupingDao.findByGroupingName(groupingName);
        user.getGroupings().add(grouping);
        grouping.getUsers().add(user);
        userDao.save(user);
        groupingDao.save(grouping);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping ("/grouplistforuser")
    @ResponseBody
    public Iterable<Grouping> getAllGroupingsForUser(String userName) {
        User user = userDao.findByUserName(userName);
        return user.getGroupings();
    }
}
