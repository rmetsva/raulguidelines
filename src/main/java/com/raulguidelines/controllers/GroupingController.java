package com.raulguidelines.controllers;

import com.raulguidelines.models.Grouping;
import com.raulguidelines.models.GroupingDao;
import com.raulguidelines.models.User;
import com.raulguidelines.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PreRemove;
import java.util.Set;

/**
 * Created by raul.metsva on 31.03.2017.
 */
@Controller
public class GroupingController {

    @Autowired
    private GroupingDao groupingDao;

    @Autowired
    private UserDao userDao;

    @CrossOrigin(origins = "*")
    @RequestMapping("/grouplist")
    @ResponseBody
    public Iterable<Grouping> getAllGroupings() {
        return groupingDao.findAll();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/create-group")
    @ResponseBody
    public void createGrouping(String groupingName) {
        Grouping grouping = new Grouping(groupingName);
        groupingDao.save(grouping);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/userlistforgroup")
    @ResponseBody
    public Iterable<User> getAllUsersForGrouping(String groupingName) {
        Grouping grouping = groupingDao.findByGroupingName(groupingName);
        return grouping.getUsers();
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete-group")
    @ResponseBody
    public void deleteGrouping(String groupingName) {
        Grouping grouping = groupingDao.findByGroupingName(groupingName);
        //grouping.getUsers().clear();
        //groupingDao.save(grouping);
        groupingDao.delete(grouping);
    }

}

/*
Iterable<User> users = userDao.findAll();
        for (User user : users) {
            for (Grouping grouping1 : user.getGroupings()) {
                if (grouping1.getGroupingName() == groupingName) {
                    user.getGroupings().remove(groupingDao.findByGroupingName(groupingName));
                    //User user1 = userDao.findByUserName(user.getUserName());
                    //user1.getGroupings().remove(groupingDao.findByGroupingName(groupingName));
                    userDao.save(user);
                }
            }
        }*/