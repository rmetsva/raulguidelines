package com.raulguidelines;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RaulguidelinesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaulguidelinesApplication.class, args);
	}
}
