package com.raulguidelines.storage;

import com.raulguidelines.models.MyFile;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by raul.metsva on 13.04.2017.
 */
public interface StorageService {

    void init();

    String store(MultipartFile file);

    List<MyFile> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

    void saveDescription(String description, File file);

}
