package com.raulguidelines.storage;

import com.raulguidelines.models.MyFile;
import com.raulguidelines.models.MyFileDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raul.metsva on 13.04.2017.
 */
@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    @Autowired
    private MyFileDao myFileDao;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public String store(MultipartFile file) {
        String Message = "You successfully uploaded the file";
        try {
            if (file.isEmpty()) {
                Message = "Please provide a file";
                /*throw new StorageException("Failed to store empty file " + file.getOriginalFilename());*/
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            Message = "Please provide a new file!";
            /*throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);*/
        }
        return Message;
    }

    @Override
    public void saveDescription(String description, File file) {
        try {
            MyFile myFile = new MyFile(file, this.rootLocation, description);
            myFileDao.save(myFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<MyFile> loadAll() {

        List<MyFile> listOfFiles = new ArrayList<>();
        File folder = new File(this.rootLocation.toString());
        File[] arrayOfFiles = folder.listFiles();

        for(int i = 0; i < arrayOfFiles.length; i++) {
            MyFile myFile = null;
            try {
                String a = "http://localhost:8080/files/" + arrayOfFiles[i].getName();
                String description = myFileDao.findByPath(a).getDescription();
                myFile = new MyFile(arrayOfFiles[i], this.rootLocation, description);
            } catch (IOException e) {
                e.printStackTrace();
            }
            listOfFiles.add(myFile);
        }

        return listOfFiles;

        /*try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.rootLocation.relativize(path));
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }*/

        /*File folder = new File(this.rootLocation.toString());
        File[] listOfFiles = folder.listFiles();

        return Arrays.stream(listOfFiles);*/
    }



    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}
