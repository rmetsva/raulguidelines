package com.raulguidelines.storage;

/**
 * Created by raul.metsva on 13.04.2017.
 */
public class StorageException extends RuntimeException {
    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}